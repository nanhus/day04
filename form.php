<?php
	$gender = array(0 => 'Nam',1 => 'Nữ');
	$faculty = array(''=>'','MAT' => 'Khoa học máy tính','KDL'=>'Khoa học vật liệu');
    $errors = [];
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    if (empty(trim($_POST['username']))) {
        $errors['username']['required'] = 'Hãy nhập tên.';
    }
    if (empty($_POST['gender'])) {
        $errors['gender']['required'] = 'Hãy chọn giới tính';
    }
    if (empty($_POST['faculty'])) {
        $errors['faculty']['required'] = 'Hãy chọn phân khoa.';
    }
    if (empty($_POST['birthday'])) {
        $errors['birthday']['required'] = 'Hãy nhập ngày sinh';
    }
}

echo
    "
		<head>
            <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
			<link rel='stylesheet' type='text/css' href='form.css'>
		</head>
		
		<body>
            <div>
                <form method='post'>
                    <div>
                        <div id='input'>
                            <label id='label'>Họ và tên <span>*<span></label>
                            <input type='text' name='username'/>
                            <br/>";
                                if (!empty( $errors['username']['required'])) {
                                    echo '<span id=\'errors\'>'.$errors['username']['required'].'</span>';
                                }
                        echo"    
                        </div>
                        
                        <div id='input'>
                            <label id='label'>Giới tính<span>*<span></label>";
                            for($i=0;$i<=1;$i++){
                                echo
                                "<input type='radio' name='gender'> <label id='gender'>".$gender[$i]."</label>";
                            }
                        echo "</br>";
                            if (!empty( $errors['gender']['required'])) {
                                echo '<span id=\'errors\'>'.$errors['gender']['required'].'</span>';
                            }
                            
                        echo
                        "</div>
                        <div id='input'>
                            <label id='label'>Phân khoa<span>*<span></label>
                            <select name='faculty'>
                                <option></option>";
                                foreach ($faculty as $item => $label) {
                                    echo "<option>" . $label . "</option>";
                                }

echo"
                            </select>
                            </br>";
                            if (!empty( $errors['faculty']['required'])) {
                                echo '<span id=\'errors\'>'.$errors['faculty']['required'].'</span>';
                            }
echo"
                        </div>
                        <div id='input'>
                                <label id='label'> Ngày sinh<span>*<span></label>
                                <input id='birthday' type ='date' name='birthday' placeholder='dd/mm/yyyy'/>
                                </br>";
                                if (!empty( $errors['birthday']['required'])) {
                                    echo '<span id=\'errors\'>'.$errors['birthday']['required'].'</span>';
                                }
echo "
                        </div>

                        <div id='input'>
                            <label id='label'> Địa chỉ</label>
                            <input type='text' id ='address' name='address'>
                            </br>
                        </div>
                    </div>

                    <div>
                        <button type='submit'>Đăng ký</button>
                    </div>

                </form>
            </div>
		</body>
	"
?>